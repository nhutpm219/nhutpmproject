terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.73.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {    
  features {}
  client_id= "b9c69a38-eb16-4a6f-bb22-672966fccfc9"
  client_secret= var.password
  subscription_id= "98a07b16-0c5e-4450-bbbc-80142b9477ee"
  tenant_id= "34f9664a-dd78-4e59-8208-abf0ae686499"
}
